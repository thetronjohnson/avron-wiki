# Building neovim from source on Debian GNU/Linux

If you're using Debian and want the latest version of neovim, at the moment
your best bet is to use a flatpak. But if you want to use neovim, I'm going to
assume that installing Neovim with Flatpak repulses you to your core. After a
couple of frustrated google searches, I went through the wiki and found
something that worked. I have put it here so that I know exactly where to look
when I wish to build neovim again. So here we are. Obviously we should be
scraping through the official wiki to figure this out but we have things to do.
The instructions below can be used to build the master branch of Neovim on
Debian release "Bookworm" (and possibly trixie).

- Clone the repository
```sh
git clone https://github.com/neovim/neovim.git
```
- Install dependencies.
```sh
sudo apt install luajit libluajit-5.1-dev lua-mpack lua-lpeg libunibilium-dev libmsgpack-dev libtermkey-dev
```
- Build deps
```sh
mkdir .deps && cd .deps
cmake ../cmake.deps/ -DUSE_BUNDLED=OFF -DUSE_BUNDLED_LIBUV=ON -DUSE_BUNDLED_LUV=ON -DUSE_BUNDLED_LIBVTERM=ON -DUSE_BUNDLED_TS=ON -G Ninja
ninja
```
- Build neovim
```sh
cd .. && mkdir build && cd build
cmake ..
make
sudo make install
```
